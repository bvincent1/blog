ifndef CI
	include .env
endif

run:
	hugo server -D

deploy: build
	aws s3 sync public/ s3://$(AWS_BUCKET)

build:
	hugo

build-drafts:
	hugo -D
