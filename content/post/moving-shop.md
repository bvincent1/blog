+++
title = "Moving Shop"
date = 2017-12-08T00:52:37-07:00
description = "Moving from Github Pages to Gitlab with CI deployment to S3 with Cloudfront CDN"
toc = true
categories = ["technology"]
tags = ["gitlab", "AWS", "CI", "blog"]
+++

# Why the change?

Simple answer: Because I can’t style anything worth a damn, but I really like hacker themes.

Basically I just like shiny things and keep moving from one theme to another since I can’t style my own. So when I casually look up blog themes and come across one that I like I’m basically shoehorned into taking all my pre existing content and reformatting it into this new setup. Classic developer problem. We love shiny, and new, but constantly overlook our current problems. More on that later.

Here's the [Repo Link](https://gitlab.com/bvincent1/blog) because I for one appreciate looking at the code while reading the post.
<!--more-->

# hugo new site

Hugo is a site building framework written in go. I don’t really care since I’ve already put the cart before the horse by focusing on look, not functionality, portability, or simplicity. So when I read the install and see that all I need is a working go runtime, all I can think is “now I have to install stuff”. To install this new thing all I need is
```bash
sudo apt-get install hugo

```
and away I go. Now I can quickly spin up a new blog with

```bash
hugo new site blog

```

which creates a nice site layout in the `blog` folder. A quick look at the layout shows this nice looking tree

```bash
├── archetypes
├── config.toml
├── content
│   └── post # markdown posts will go here
├── data
├── layouts # custom overrides
├── static # static content eg: 'blah.com/static_stuff_located_here.js'
└── themes # theme install folder

```

Next up we need to install our theme.

According to the [Quick Start](https://gohugo.io/getting-started/quick-start/#step-3-add-a-theme) we should clone a submodule into the themes folder. This seems like a good idea until you realize that to make changes you have to use overrides in your own blog files since hugo gives those priority over the ones in the theme. However chances are you only want to change some small thing, but you’ll have to copy the whole file and mirror any folder structure just to make your change. This leads you to the solution of making a new fork! This is probably a good idea for larger teams or sites since you can split the development of the site and content across 2 repositories. However it turns out that for my specific purpose of a 1 man blog this is all overkill.

```bash
cd theme/
curl https://github.com/whomever/theme.zip
unzip theme-master.zip
echo ‘theme = theme-master’ >> ../config.toml

```
In the end I just downloaded the theme and unzipped it into the folder as is. This lets me make direct changes to the layout and view without having to update 2 different repos. I’d absolutely recommend this as a starting point since most people won’t need the extra fluff or complexity that the previous 2 solutions add. 1 more thing and then we're good to work.

Let's set some config files.

```toml
baseurl = "https://blog.hacknslash.io" # Controls base URL
languageCode = "en-US" # Controls site language
title = "After Dark" # Homepage title and page title suffix
paginate = 5 # Number of posts to show before paginating

theme = "after-dark-master" # Set default theme

enableRobotsTXT = true # Suggested, enable robots.txt file
googleAnalytics = "" # Optional, add tracking Id for analytics
disqusShortname = "" # Optional, add Disqus shortname for comments
SectionPagesMenu = "main" # Enable menu system for lazy bloggers
footnoteReturnLinkContents = "↩" # Provides a nicer footnote return link
pygmentsCodeFences = true # adds GitHub style code highlights

[params]
  description = "" # Suggested, controls default description meta
  author = "Ben Vincent" # Optional, controls author name display on posts
  hide_author = false # Optional, set true to hide author name on posts
  show_menu = false # Optional, set true to enable section menu
  powered_by = true # Optional, set false to disable credits
  theme_variant = "" # Optional, for use to overriding default theme

```

So I'm just gonna leave this here with the mention that in my case the themes submodule needed to be changed similarly so as to fix site links. Might have just been because of how mine was set up, but maybe not. Good luck.

# CI for a change

So all this has been cool and all and now I have a cool new site, but only one place to build it from. Sounds familiar. I still haven't solved my first problem of how to make sure that I can always build. I'm still tied to a single machine.

Many Google searches later and free tier comparisons and we arrive at

<img src="/images/gitlablogo.svg" alt="gitlab" width="700">



So now we add a `gitlab-ci.yml` file to the root to manage our CI settings and scripts.

```yaml
# .gitlab-ci.yml
image: monachus/hugo

production:
  type: deploy
  before_script:
  - apt-get update
  - apt-get install build-essential awscli -y
  script:
  - make deploy
  artifacts:
    paths:
    - public
  only:
  - master

```

Now there's a few things about this that aren't straight forwards so I'll focus on that and leave the rest for you code types to figure out.

First off I'm storing my environment variables in two places. First locally in my `.env`, and secondly here in gitlab. There's a repo setting for CI and in that there's a secret keys section. According to the documentation I can safely store my variables in there any they won't become public to anyone outside a user group of my choosing. A little further reading into the docs and you can find all sorts of goodies exposed to the build environment and how to override them if needed. I'm not covering that, so have fun.

Secondly we have to make sure that the build directory, in this case `public/` is kept at the end of the build and deployed somewheres… (perfect segway into)


# S3, the online endpoint of choice by devs everywhere (and those who are just looking for free tier stuff)

Moving from github pages to S3 is pretty much a no brainer for all the reasons. It offers so many more built in tools and resources than pages, and it’s so cheap at my level that it may as well be free that I can’t imagine I’ll ever really go back. Simply go to the S3 dashboard, create a new bucket, and make sure to name it the same as what our site domain will be (ie ‘blog.hacknslash.io’ in my case). Leave everything else default since we’ll override that permanently next with the access policy.

The access policy is a simple read access setting that should be done via the bucket policy tab in the bucket menu. If done outside the menu, the policy will get rest each time any files is changed.

```json
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "AllowPublicRead",
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::blog.hacknslash.io/*"
        }
    ]
}
```

Here’s an example policy for my blog that allows anyone to perform a get on the files of my blog. Obviously if someone else was using it, they’d need to change the bucket value, but everything else should stay the same.

*Heads up* I had to use this version date (ie not the current date) or else my policy wouldn’t work. I haven’t found any reason why this is, but I’ll add an update when I do.

In addition to the policy, you probably wanna set go ahead and hit the properties tab and enable static site hosting and set your `index.html`. You can leave the `404` blank if you want, or create a flashy page easter egg type page like all the big tech companies do.

Last of the settings should be logging. Logging lets you designate a bucket to write the access logs into for later (but probably never) use. Ideally you’d wanna pair this with some front end analytics (GA) that way you can match and verify the accuracy of either / both of them. You’ll have to set up a second bucket before you add logging (make sure to enable S3 logging permissions), and you can prefix the logs if you wanted to have one log bucket for everything. With that things are pretty much wrapped up here.

Now if you click on the bucket there should be some link that you can open to visit your beautiful site! Only issue is that it’s not a pretty url, and it’s not going to be `https`.

# putting the ‘s’ in ‘https’

First off in order to add encryption to our site, we’ll need a certificate. There exists a few solutions to this part, but I’m going to use the certificate manager since we’ve been fairly AWS reliant so far, and I see no need to break the trend. Also still free (or super cheap since at this point I’m not differentiating between the two). The instructions are super simple with the only important decision being choosing our certificate domain. In my case I made a root certificate for all my subdomains, `*.hacknslash.io`. This means that I can use this to encrypt all my other subdomains under one certificate and really just saves me time from having to make more than one. I’m not certain there are drawbacks to this, but I’d certainly love to be educated about them if there are. [Educate me](mailto:vincent@hacknslash.io). Now we’ll attach this certificate to our bucket with a CDN.


Go ahead and navigate to the cloudfront console (you’ll to type it in in the search bar since either I can find it, or it’s not listed in that huge view of services) and create a new distribution.
This part is actually the easiest to set up, but it has the most important consequences.

<img src="/images/cloudfrontconsole.png" alt="Cloudfront settings console" width="700">

Literally the only thing you’ll HAVE to change is the certificate settings, everything else is optional.

I’m going to go ahead and say that the default settings will fit most anyone's needs pretty well and that the only important change will be the TTL. The TTL will determine how it takes for any changes (ie a new post) to go live. This delay means that you SHOULD NOT DEPLOY WITHOUT KNOWING THAT IT’LL WORK. Otherwise you’ll probably wait the 24 hours and then forget what you changed and have to spend another 24 hours waiting for you fix to go through. Without mentioning the possibility of adding a staging env, I will mention that there is a way to manually reset this cache (ie cache busting), but it will cost you money, eventually, after enough uses.

In my CI config I omit the staging env since I can simply do local testing and I’m confident that static sites are pretty resilient to my limited changes and posts. If that wasn’t the case, I’d absolutely set up a staging site on gitlab pages and add a staging branch for this to build off.

```yaml
# .gitlab-ci.yml
image: monachus/hugo


production:
  type: deploy
  before_script:
  - apt-get update
  - apt-get install build-essential awscli -y
  script:
  - make deploy
  artifacts:
    paths:
    - public
  only:
  - master
```
See [this](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) for a nice example of how one might set up a nice staging env.

*EDIT* I absolutely added a staging pages setup, but it turns out that my site has some broken links on this. No clue why, so I'm leaving this up to the readers.

Leaving the rest unchanged, we can finish and after a few minutes (almost 10mins for me) see the lovely green “enabled” text for our distribution state thereby confirming our successful implementation of a CDN. ONE LAST THING.

# the internet really is best described as a series of tubes

With the CDN out of the way, we can move to the easiest part of our journey, setting up the domain name. Again, I use AWS so this part takes place in Route 53, but the principle should be fairly translatable (but maybe just a little harder for others). Create an “A Record” and click alias to tell AWS to list out all the possible endpoints that we could use. We Should see both our S3 bucket pop up and our cloudfront distribution, but we should only pick the later since that’s where the certificate is located. Easy peasy.

With all this configuration it’s really easy to picture a bunch of tubes feeding information from our code to reader’s computer. It’s really just a long chain of endpoints that just seem to work without much more effort on our part. This is exactly how code should and why all this setup is justified since we’ll never have to do it again.

<div style='position:relative;padding-bottom:54%'><iframe src='https://gfycat.com/ifr/VigorousSoupyJackrabbit' frameborder='0' scrolling='no' width='100%' height='100%' style='position:absolute;top:0;left:0' allowfullscreen></iframe></div>

Ha. There’s a nice pipe dream (cause pipe jokes now).

# EDIT: Bonus step!!

So after I deployed and got this working I found out that my post links were broken. After some problem solving I found out that Cloudfront doesn't support directory `index` files. It turns out that I can only enable this sort of behavior for the root directory and after that, it must be directly linked. After several days of on-and-off researching and tinkering I found a solution that almost borders on magic because of how spooky it seems.

<div style='position:relative;padding-bottom:54%'><iframe src='https://gfycat.com/ifr/ImmenseEducatedDodo' frameborder='0' scrolling='no' width='100%' height='100%' style='position:absolute;top:0;left:0' allowfullscreen></iframe></div>

Enter Lambda Edge! A special form of lambda methods that live on the CDN edge servers! It turns out that the best way to fix my problem is to add a lambda function that re-writes the incoming request to include the `index.html`. To add this, go ahead and create a lambda function with the following settings:

When promted for the permissions use:
```json
{
  "roleName": "LambdaEdgeRole",
  "policies": [
    {
      "document": {
        "Version": "2012-10-17",
        "Statement": [
          {
            "Effect": "Allow",
            "Action": [
              "logs:CreateLogGroup",
              "logs:CreateLogStream",
              "logs:PutLogEvents"
            ],
            "Resource": [
              "arn:aws:logs:*:*:*"
            ]
          }
        ]
      },
      "name": "AWSLambdaEdgeExecutionRole-50105366-5439-4539-95b2-1ff8adf04632",
      "id": "ANPAJBSHGMQPVMHRUUDMY",
      "type": "managed",
      "arn": "arn:aws:iam::566572366192:policy/service-role/AWSLambdaEdgeExecutionRole-50105366-5439-4539-95b2-1ff8adf04632"
    }
  ]
}
```
This will preconfigure the resource logs but, you'll still need to enable the trigger and the actual function.
For trigger use:

```yaml
Distribution Id: your_cloudfront_arn
Cache Behavior: *
Cloudfront Event: Origin Request
```

And for the code use:

```js
'use strict';
exports.handler = (event, context, callback) => {

    // Extract the request from the CloudFront event that is sent to Lambda@Edge
    var request = event.Records[0].cf.request;

    // Extract the URI from the request
    var olduri = request.uri;

    // Match any '/' that occurs at the end of a URI. Replace it with a default index
    var newuri = olduri.replace(/\/$/, '\/index.html');

    // Log the URI as received by CloudFront and the new URI to be used to fetch from origin
    console.log("Old URI: " + olduri);
    console.log("New URI: " + newuri);

    // Replace the received URI with the URI that includes the index page
    request.uri = newuri;

    // Return to CloudFront
    return callback(null, request);

};
```

*TLDR* We actually reached the limits of cloudfront and had to add some special request modifying methods to add directory indexes.

# conclusion

At the end of all this we should have a very functional and easy to use blog that solves most of our coding and development problems, without causing any new ones. No more random installs on random machines and no more lazy sites that don’t have simple logging or encryption. Yes we paid for it now, but the benefits far outweigh these new costs. At larger scales this work will pay off handsomely by solving problems that we already have now at low scale. Ideally we’d have just done this from the start, but I think that we all realize that we need to build it wrong a few times to learn what we need to build it right.
